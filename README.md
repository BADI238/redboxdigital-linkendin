# Magento Linkendin Extention #
This Extention is for getting linkendin Url while registering user.
## What it does ##
* This module add a new custom attribute to get customer linkendin url while registration. 
* Admin can edit this attribute value from backend. 
* User also can edit this value in account information.
* Also admin can set this attribute required or not from admin panel configuration.
# Installation #
## Manually ##
To install this package manually you need access to your server file system . And take the following steps:
Download the zip file from the Bitbucket repository.
Upload the contents to <your magento path>/
Now you can see new attribute in your registration form also in admin customer creation form.

* Goto Admin > configuration > REDBOXDIGITAL > Settings You can set this attribute require or not.

# License #
This module is distributed under the following licenses:
### Obaid Free License v. 1.0 ###
# Authors #
Obaid ur Rehman
# About Obaid #
Obaid is Magento Developer have good exprience in Magento. For More information

[Who Obaid Is.?](http://obaidabbasi.me)